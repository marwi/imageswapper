public class Collage
{
	String imgSource, imgTarget;
	private Analyzer source, target;
	private int swap = 0;

	Collage()
	{
		FileDialogue();
	}

	protected void FileDialogue()
	{
		selectInput("Select a file to process:", "fileSelected", null, this);
	}

	public void fileSelected(File selection) 
	{
		if (selection == null) 
		{
			println("Window was closed or the user hit cancel.");
		} 
		else 
		{
			println("User selected " + selection.getAbsolutePath());
			String path = selection.getAbsolutePath();
			if(IsImage(path))
			{
				if(imgSource == null)
					imgSource = path;
				else if (imgTarget == null)
					imgTarget = path;

				if(imgTarget == null)
				{
					FileDialogue();
				}
				else 
					Analyze(imgSource, imgTarget);
			}
		}
	}

	protected boolean IsImage(String path)
	{
		if(path.endsWith(".jpg"))
			return true;
		else if(path.endsWith(".png"))
			return true;
		else 
			return false;
	}

	protected void Analyze(String imgSource, String imgTarget)
	{
		source = new Analyzer(imgSource);
		source.Run();
		source.draw();

		target = new Analyzer(imgTarget);
		target.Run();
		target.draw();

		source.draw(target);
	}

	protected void Swap()
	{
		if(source == null || target == null)
		{
			println("Load source and target images first (spacebar)");
			return;
		}
		
		if(swap % 2 == 0)
			source.draw(target);
		else
			target.draw(source);

		swap ++;
	}
}