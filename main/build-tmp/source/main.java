import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.Collections; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class main extends PApplet {



Collage collage;

public void setup()
{
	size(900,650);
	background(0);
}

public void keyPressed()
{
	switch(key)
	{
		case ' ':
		collage = new Collage();
		break;

		case 's':
		collage.Swap();
		break;
	}
}

public void draw()
{

}
public class Analyzer
{
	String path;
	ArrayList<Piece> pieces;

	public Analyzer(String path)
	{
		this.path = path;
		pieces = new ArrayList<Piece>();
	}

	public void Run()
	{
		PImage img = loadImage(path);
		img.resize(width,height);
		//image(img, 0, 0);
		background(0);

		img.loadPixels();

		int count = 30;
		int xStep = width / count;
		int yStep = height / count;

		for(int x = 0; x + xStep <= width; x += xStep)
		{
			for(int y = 0; y + yStep <= height; y += yStep)
			{
				Piece p = Get(img, x, y, xStep, yStep);
				pieces.add(p);
			}
		}

		Collections.sort(pieces);
	}

	protected Piece Get(PImage img, int x, int y, int w, int h)
	{
		Piece p = new Piece(x, y, w, h);

		String he = hex(img.pixels[y*img.width+x]);

		for(int yi = y; yi < y+h; yi++)
		{
			for(int xi = x; xi < x+w; xi++)
			{
				int c = img.pixels[yi*img.width+xi];
				p.Add(c, xi, yi);
			}
		}

		stroke(0);
		fill(unhex(he));
		rect(x,y,3,3);

		return p;
	}

	public void draw()
	{
		draw(pieces, pieces);
	}

	public void draw(Analyzer other)
	{
		draw(pieces, other.pieces);
	}

	protected void draw(ArrayList<Piece> image, ArrayList<Piece> position)
	{
		for(int i = 0; i < image.size(); i++)
		{
			Piece p = (Piece) image.get(i);
			PImage nimg = p.GetImage();

			Piece pos = (Piece) position.get(i);

			image(nimg, pos.x, pos.y); //mx, random(0, height));
		}
	}

	protected void PrintBrightnesses()
	{
		for(Piece p : pieces)
		{
			println("Brightness: " + p.CalcBrightness());
		}
	}

}
public class Collage
{
	String imgSource, imgTarget;
	private Analyzer source, target;
	private int swap = 0;

	Collage()
	{
		FileDialogue();
	}

	protected void FileDialogue()
	{
		selectInput("Select a file to process:", "fileSelected", null, this);
	}

	public void fileSelected(File selection) 
	{
		if (selection == null) 
		{
			println("Window was closed or the user hit cancel.");
		} 
		else 
		{
			println("User selected " + selection.getAbsolutePath());
			String path = selection.getAbsolutePath();
			if(IsImage(path))
			{
				if(imgSource == null)
					imgSource = path;
				else if (imgTarget == null)
					imgTarget = path;

				if(imgTarget == null)
				{
					FileDialogue();
				}
				else 
					Analyze(imgSource, imgTarget);
			}
		}
	}

	protected boolean IsImage(String path)
	{
		if(path.endsWith(".jpg"))
			return true;
		else if(path.endsWith(".png"))
			return true;
		else 
			return false;
	}

	protected void Analyze(String imgSource, String imgTarget)
	{
		source = new Analyzer(imgSource);
		source.Run();
		source.draw();

		target = new Analyzer(imgTarget);
		target.Run();
		target.draw();

		source.draw(target);
	}

	protected void Swap()
	{
		if(source == null || target == null)
		{
			println("Load source and target images first (spacebar)");
			return;
		}
		
		if(swap % 2 == 0)
			source.draw(target);
		else
			target.draw(source);

		swap ++;
	}
}
class Piece implements Comparable<Piece>
{
	int x, y, w, h;
	ArrayList<String> colors;
	float averageBrightness;


	Piece(int x, int y, int w, int h)
	{	
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;

		colors = new ArrayList<String>();
	}

	public void Add(int c, int x, int y)
	{
		colors.add(hex(c));
	}

	public float CalcBrightness()
	{
		float total = 0;

		for(String s : colors)
		{
			int c = unhex(s);
			float b = brightness(c);
			total += b;
		}

		total /= colors.size();
		averageBrightness = total;

		return averageBrightness;
	}

	public int compareTo(Piece otherPiece)
	{
		return (int)(100*(otherPiece.CalcBrightness() - CalcBrightness()));
	}

	public PImage GetImage()
	{
		PImage img = createImage(w, h, RGB);
		img.loadPixels();
		int i = 0;
		while(i < img.pixels.length && i < colors.size())
		{
			img.pixels[i] = unhex(colors.get(i));
			i++;
		}
		return img;
	}
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "main" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
