class Piece implements Comparable<Piece>
{
	int x, y, w, h;
	ArrayList<String> colors;
	float averageBrightness;


	Piece(int x, int y, int w, int h)
	{	
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;

		colors = new ArrayList<String>();
	}

	public void Add(color c, int x, int y)
	{
		colors.add(hex(c));
	}

	public float CalcBrightness()
	{
		float total = 0;

		for(String s : colors)
		{
			color c = unhex(s);
			float b = brightness(c);
			total += b;
		}

		total /= colors.size();
		averageBrightness = total;

		return averageBrightness;
	}

	int compareTo(Piece otherPiece)
	{
		return (int)(100*(otherPiece.CalcBrightness() - CalcBrightness()));
	}

	public PImage GetImage()
	{
		PImage img = createImage(w, h, RGB);
		img.loadPixels();
		int i = 0;
		while(i < img.pixels.length && i < colors.size())
		{
			img.pixels[i] = unhex(colors.get(i));
			i++;
		}
		return img;
	}
}
