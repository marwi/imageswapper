public class Analyzer
{
	String path;
	ArrayList<Piece> pieces;

	public Analyzer(String path)
	{
		this.path = path;
		pieces = new ArrayList<Piece>();
	}

	public void Run()
	{
		PImage img = loadImage(path);
		img.resize(width,height);
		//image(img, 0, 0);
		background(0);

		img.loadPixels();

		int count = 30;
		int xStep = width / count;
		int yStep = height / count;

		for(int x = 0; x + xStep <= width; x += xStep)
		{
			for(int y = 0; y + yStep <= height; y += yStep)
			{
				Piece p = Get(img, x, y, xStep, yStep);
				pieces.add(p);
			}
		}

		Collections.sort(pieces);
	}

	protected Piece Get(PImage img, int x, int y, int w, int h)
	{
		Piece p = new Piece(x, y, w, h);

		String he = hex(img.pixels[y*img.width+x]);

		for(int yi = y; yi < y+h; yi++)
		{
			for(int xi = x; xi < x+w; xi++)
			{
				color c = img.pixels[yi*img.width+xi];
				p.Add(c, xi, yi);
			}
		}

		stroke(0);
		fill(unhex(he));
		rect(x,y,3,3);

		return p;
	}

	public void draw()
	{
		draw(pieces, pieces);
	}

	public void draw(Analyzer other)
	{
		draw(pieces, other.pieces);
	}

	protected void draw(ArrayList<Piece> image, ArrayList<Piece> position)
	{
		for(int i = 0; i < image.size(); i++)
		{
			Piece p = (Piece) image.get(i);
			PImage nimg = p.GetImage();

			Piece pos = (Piece) position.get(i);

			image(nimg, pos.x, pos.y); //mx, random(0, height));
		}
	}

	protected void PrintBrightnesses()
	{
		for(Piece p : pieces)
		{
			println("Brightness: " + p.CalcBrightness());
		}
	}

}